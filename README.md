# CubeBot v2

A python rewrite of [CubeBot](https://gitlab.com/cubekrowd/system/cubebot)
for CubeKrowd.

## Setup

1. **Python 3.8**

   Python 3.8 is required to run the bot.

2. **Dependencies**

   Install the dependencies using `pip install -U -r requirements.txt`

3. **Configuration**

   Copy the file `sample-config.yaml` into one named `config.yaml`
   
4. **Run the bot**

   Execute `python -m cubebot`.

## API/Communication

To write a client that talks to this bot to linkup information you'll need to connect via TCP using the data specified in `config.yaml`. There are different types of data to send and are documented below.

An already implemented CubeBot2 client is available as the
[HeyCubeBot](https://gitlab.com/cubekrowd/plugins/heycubebot)
BungeeCord plugin.

### Rank transfer

Whenever rank data needs to be transfer you create a list split by `&`'s containing rank names specified in `config.yaml`. Example:

```
admin&guest&3&builder
```

### Errors

Instead of the usual return stream, some messages can return an error. If the arguments are invalid, `ERROR:ARGS` is returned. If some unspecified error occurs, `ERROR:UNKNOWN` is returned. Messages may also return other errors; see below.

### Linkup

This will store the necessary data to linkup UUID and Discord ID. It will generate a code automatically and return it to the stream. Message should be formatted as:

```
LINK:<UUID>|<Ranks>|<IGN>|<Username#0000>
```
Return stream:

```
CODE:<Code>
```
or if the UUID is already linked:

```
LINKED:<Name>
```
where the name is the linked Discord username, i.e. `xxx#yyyy`, or the Discord ID if the username couldn't be determined. Returns `ERROR:ARGS` if the arguments are invalid, `ERROR:INCORRECT_USER` if the discord user couldn't be found, and `ERROR:UNKNOWN` for any other error.

### Updating Data

This is similar to linkup transfer, but doesn't need linkup code. Formatting:

```
DATA:<UUID>|<Ranks>|<IGN>
```
Returns `OK` on success.

### Unlinking User

Similar to how updating works, but all we need is the UUID for ingame unlinking. Formatting:

```
UNLINK:<UUID>
```
The return stream is:

```
UNLINKED:<Name>
```
where the name is the unlinked Discord username. If the UUID wasn't linked, this returns `ERROR:NOTLINKED`.
