import aiohttp
from collections import OrderedDict
import discord
from discord.ext import commands

import cubebot

# a list of (original, copied) message ID pairs ordered by last access time.
copied_messages = OrderedDict()


class WebhookReader(commands.Cog):
    # This class implements a discord channel reader that sends updates from
    # channels to webhooks. The bot must obviously be in the same server as the
    # source channels to read them, but the number of sources is otherwise
    # unlimited.

    def __init__(self):
        super(commands.Cog).__init__()
        self.session = aiohttp.ClientSession()
        self.webhook = discord.Webhook.from_url(cubebot.config['webhook-url'], adapter=discord.AsyncWebhookAdapter(self.session))


    async def close(self):
        await self.session.close()


    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        # only accept from the following sources
        if message.channel.id not in cubebot.config['dev-feed-sources']:
            return

        await self.copy_message(message)


    @commands.Cog.listener()
    async def on_message_edit(self, before, after):
        # only accept from the following sources
        if after.channel.id not in cubebot.config['dev-feed-sources']:
            return

        # check LRU cache for original id
        if after.id in copied_messages:
            copied_messages.move_to_end(after.id)
            if await self.edit_message(copied_messages[after.id], after):
                return

        # if we could not find the message id in the cache, or if the
        # copied message does not exist, fall back to creating a new one.
        await self.copy_message(after)


    async def edit_message(self, dest_id, src):
        # attempt to edit the webhook-sent message that we have in the cache.
        # if this fails, return False so we can fall back to copying the message.
        try:
            await self.webhook.edit_message(dest_id, content=src.content, embeds=src.embeds)
            return True
        except:
            return False


    async def copy_message(self, message: discord.Message):
        # send copy of message
        copied = await self.webhook.send(message.content, wait=True, username='[CubeBot] '+message.author.name, avatar_url=message.author.avatar_url, embeds=message.embeds)

        # now that we have copied it, put (original, copied) at the head of the
        # LRU cache, and enforce the cache limit.
        copied_messages[message.id] = copied.id
        if len(copied_messages) > cubebot.config['webhook-reader-cache-size']:
            copied_messages.popitem(last=False)


