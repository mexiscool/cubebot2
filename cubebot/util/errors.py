class MemberNotFound(Exception):
    """Raised when a searching with Username#0000 finds nothing"""
    pass


class RankNotFound(Exception):
    """Raised when an unknown rank is referenced"""
    pass


class UUIDNotFound(Exception):
    """Raised when an unknown UUID gets processed."""


class AlreadyLinked(Exception):
    """Raised when there is an attempt to link an already linked discord user."""
