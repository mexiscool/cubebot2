import math
from datetime import datetime

import discord
from discord.ext import commands

import cubebot
from .linkup import Linkup
from .webhook_reader import WebhookReader
from .utility import Utility
from .util import Context

description = "Utility bot for CubeKrowd"


class Bot(commands.Bot):

    def __init__(self):
        allowed_mentions = discord.AllowedMentions(roles=False, everyone=False, users=True)

        intents = discord.Intents.default()
        intents.members = True
        intents.guilds = True
        super().__init__(command_prefix="!", intents=intents, description=description,
                         case_insensitive=True, allowed_mentions=allowed_mentions)

        self.boot = datetime.now()

        self.add_cog(Linkup(self))
        self.add_cog(Utility())

        self.webhook_reader = WebhookReader()
        self.add_cog(self.webhook_reader)

    async def login(self, *args, **kwargs):
        await super().login(cubebot.config['bot_token'], bot=True)

    async def connect(self, *args, **kwargs):
        await super().connect(reconnect=True)

    async def clean_webhooks(self):
        await self.webhook_reader.close()

    async def on_ready(self):
        print(f"{self.user} has connected to Discord!")
        await self.change_presence(activity=discord.Game(name=cubebot.config.get('playing', 'on CubeKrowd :)')))

    async def on_member_remove(self, member):
        cubebot.link_manager.remove_data(member)

    async def on_member_update(self, before, after):
        ign = cubebot.link_manager.get_ign(after)
        # If IGN is none it means they're not in the db.
        # This prevents an endless call of trying to set their display_name to None.
        if ign is not None and after.display_name != ign:
            try:
                await after.edit(nick=ign, reason="CubeBot fix name")
            except Exception as e:
                print(f"Failed to fix name for {after}: {e}")

    async def on_user_update(self, before, after):
        ign = cubebot.link_manager.get_ign(after)
        # If IGN is none it means they're not in the db.
        # This prevents an endless call of trying to set their display_name to None.
        if ign is not None and after.display_name != ign:
            try:
                guild = self.get_guild(cubebot.config['guild-id'])
                member = guild.get_member(after.id)
                await member.edit(nick=ign, reason="CubeBot fix name")
            except Exception as e:
                print(f"Failed to fix name for {after}: {e}")

    async def on_command_error(self, ctx: Context, error):
        """
        Error handling. Helps not spam console by users and allows for better help.
        """
        if isinstance(error, commands.CommandNotFound):
            return

        if isinstance(error, commands.ArgumentParsingError):
            return await ctx.send(error, delete_after=30)
        if isinstance(error, commands.BadArgument):
            return await ctx.send(error, delete_after=30)
        if isinstance(error, commands.CheckFailure):
            return

        if isinstance(error, (commands.MissingPermissions, commands.MissingRole)):
            return await ctx.send("You don't have permission to do that!", delete_after=15)

        # If there are no commands this can be removed
        if isinstance(error, commands.CommandOnCooldown):
            if await self.is_owner(ctx.author):
                # We don't want me to be on cooldown.
                return await ctx.reinvoke()
            # Let people know when they can retry
            await ctx.message.delete()
            await ctx.send(f"You are currently on cooldown! Try again in {math.ceil(error.retry_after)}", delete_after=15)
            return

        raise error

    async def process_commands(self, message):
        if message.author.bot:
            return

        ctx: Context = await self.get_context(message, cls=Context)

        if ctx.command is None:
            return

        await self.invoke(ctx)
