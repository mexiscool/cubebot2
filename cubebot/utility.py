from discord.ext import commands

import cubebot
from cubebot.util.context import Context


class Utility(commands.Cog):

    @commands.command(name="Ping")
    @cubebot.debug_only()
    async def ping(self, ctx: Context):
        """
        Just to make sure the bot actually works.
        """
        await ctx.send("Pong")
